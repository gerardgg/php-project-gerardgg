<?php // Provides access to view ?>
<?php require_once 'Views/header.php'; ?>
<?php // Verify permissions ?>
<?php require_once 'Views/assets/permissions/verifyAdmin.php' ?>
<?php // Provides access to namespaces to work properly ?>
<?php require_once 'Logic/editData.php'; ?>
<?php // Needed namespaces for required functions or constants ?>
<?php use GoldHotel\Edit\Views\Users as EditUsersViews; ?>
<h1>Edit user</h1>
<?php $output = EditUsersViews\loadTargeEditUserData(); ?>
<?php echo $output; ?>
<?php // Provides access to view ?>
<?php require_once 'Views/footer.php'; ?>