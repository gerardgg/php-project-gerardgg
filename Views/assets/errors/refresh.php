<?php
if(!isset($_SESSION['refreshFlag'])) {
    $_SESSION['refreshFlag'] = true;
    header("Refresh:0"); 
} else {
    unset($_SESSION['refreshFlag']); 
}