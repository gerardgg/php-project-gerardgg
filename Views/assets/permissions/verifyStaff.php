<?php

// Provides access to namespaces to work properly
require_once 'Logic/generics.php';
// Needed namespaces for required functions or constants
use GoldHotel\Generics as GlobalGenerics;

if(isset($_SESSION['userInfo']['role'])) {
    if($_SESSION['userInfo']['role'] != 'staff' && $_SESSION['userInfo']['role'] != 'admin') {
        $_SESSION['permissionError'] =  GlobalGenerics\FORMEDONLYAUTHORIZEDERROR;
        \header('Location: index.php');
        exit();
    }
}

?>