<?php

// Needed namespaces for required functions or constants
use GoldHotel\Generics as GlobalGenerics;

// Provides access to namespaces to work properly
require_once 'Logic/generics.php';

if(!isset($_SESSION['userInfo']['role'])|| $_SESSION['userInfo']['role'] == 'visitor') {
    $_SESSION['permissionError'] =  GlobalGenerics\FORMEDONLYLOGGEDERROR;
    \header('Location: index.php');
    exit();

}

?>