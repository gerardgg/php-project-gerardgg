<?php // Needed namespaces for required functions or constants ?>
<?php use GoldHotel\Generics as GlobalGenerics; ?>
<?php if(isset($_SESSION['errorLogin'])): ?>
    <?= GlobalGenerics\ERRORTEXT.$_SESSION['errorLogin']['errorUsersFile'].GlobalGenerics\CLOSETEXT;  ?>
    <?php unset($_SESSION['errorLogin']) ?>
<?php endif; ?>
