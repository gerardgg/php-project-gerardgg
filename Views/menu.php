<?php 

// Needed namespaces for required functions or constants
use GoldHotel\Generics as GlobalGenerics;

$everybody = '<li><a href="index.php" class="menuItem removeDecoration">Home</a></li>'.
'<li><a href="dayMenu.php" class="menuItem removeDecoration">Day menu</a></li>';
$visitor = '<li><a href="register.php" class="menuItem removeDecoration">Register</a></li>'.
'<li><a href="login.php" class="menuItem removeDecoration">Login</a></li>';
$registrered = '<li><a href="menu.php" class="menuItem removeDecoration">View menu</a></li>'.
'<li><a href="logout.php" class="menuItem removeDecoration">Logout</a></li>';
$staff = '<li><a href="adminMenu.php" class="menuItem removeDecoration">Admin menus</a></li>';
$admin = '<li><a href="adminUsers.php" class="menuItem removeDecoration">Admin users</a></li>';

$menu = array('everybody' => $everybody, 
            'visitor' => $everybody.$visitor,
            'registrered' => $everybody.$registrered,
            'staff' => $everybody.$staff.$registrered,
            'admin' => $everybody.$admin.$staff.$registrered
        );

        
?>
<nav>
    <img src="./Images/logo.png" alt="logo" id="logo">
    <?php if(isset($_SESSION['userInfo']['username'])): ?>
        <?php $name = $_SESSION['userInfo']['name']; ?>
        <?php $surname = $_SESSION['userInfo']['surname']; ?>
        <?= GlobalGenerics\FLEXDIV.GlobalGenerics\CORRECTTEXT.'Welcome '.$name.' '.$surname.GlobalGenerics\CLOSETEXT.GlobalGenerics\CLOSEDIV ?>
    <?php endif; ?>
    <?php if(isset($_SESSION['logOut'])): ?>
        <?php $logOutResult = $_SESSION['logOut']; ?>
        <?php switch($logOutResult): 
            case true: ?>
            <?= GlobalGenerics\FLEXDIV.GlobalGenerics\CORRECTTEXT.'Succesful logout'.GlobalGenerics\CLOSETEXT.GlobalGenerics\CLOSEDIV ?>
            <?php break; ?>
            <?php case false: ?>
                <?= GlobalGenerics\FLEXDIV.GlobalGenerics\ERRORTEXT.'Unsuccesful logout'.GlobalGenerics\CLOSETEXT.GlobalGenerics\CLOSEDIV ?>
            <?php break; ?>
        <?php endswitch; ?>
        <?php unset($_SESSION['logOut']); ?>
        <?php if (ini_get("session.use_cookies")): ?>
            <?php $params = session_get_cookie_params(); ?>
            <?php setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"],$params["httponly"]); ?>
        <?php endif; ?>
    <?php endif; ?>
    <ul class="menu">
        <?php if(isset($_SESSION['userInfo'])): ?>
            <?php $role = $_SESSION['userInfo']['role']; ?>
            <?php echo $menu[$role]; ?>
        <?php endif; ?>
    </ul>
</nav>