<?php // Provides access to namespaces to work properly ?>
<?php require_once 'Logic/users.php';?>
<?php // Needed namespaces for required functions or constants ?>
<?php use GoldHotel\Generics as GlobalGenerics; ?>
<?php use GoldHotel\Users\Register as Register; ?>
<div class="loginForm">
        <h2>Register</h2>
        <?php if(isset($_SESSION['registerCompleted'])):?>
            <?= GlobalGenerics\CORRECTTEXT.$_SESSION['registerCompleted'].GlobalGenerics\CLOSETEXT ?>
            <?php unset($_SESSION['registerCompleted']); ?>
        <?php endif; ?>
        <form method="POST" action="<?= htmlentities($_SERVER['PHP_SELF']); ?>">
            <?php // Displays permission errors ?>
            <?php require_once 'Views/assets/login/verifyConnectionErrors.php'; ?>
            <fieldset>
                <legend>Introduce your data</legend>
                <?php $emptyFieldsFlag = isset($_SESSION['formResults']) ? true : false; ?>
                <?php if($emptyFieldsFlag && $_SESSION['formResults']['username'] == null): echo GlobalGenerics\FORMEDEMPTYUSERNAME;  endif; ?>
                <?php if(isset($_SESSION['formResults']['usedUsername'])): ?>
                    <?= GlobalGenerics\ERRORTEXT.$_SESSION['formResults']['usedUsername'].GlobalGenerics\CLOSETEXT;; ?>
                    <?php unset($_SESSION['formResults']['usedUsername']); ?>
                <?php endif; ?>
                <input type="text" name="username" placeholder="Username">
                <?php if($emptyFieldsFlag && $_SESSION['formResults']['password'] == null): echo GlobalGenerics\FORMEDEMPTYPASSWORD;  endif; ?>
                <input type="password" name="password" placeholder="Password">
                <?php if($emptyFieldsFlag && $_SESSION['formResults']['name'] == null): echo GlobalGenerics\FORMEDEMPTYNAME;  endif; ?>
                <input type="text" name="name" placeholder="Name">
                <?php if($emptyFieldsFlag && $_SESSION['formResults']['surname'] == null): echo GlobalGenerics\FORMEDEMPTYSURNAME;  endif; ?>
                <?php if(isset($_SESSION['formResults'])): ?>
                    <?php unset($_SESSION['formResults']); ?>
                <?php endif; ?>
                <input type="text" name="surname" placeholder="Surname">
                <div class="loginButtons register">
                    <?php if($_SESSION['userInfo']['role'] == 'visitor'): ?>
                        <a href="login.php" class="goldButton removeDecoration">LogIn</a>
                    <?php else: ?>
                        <input type="reset" value="Reset" class="goldButton">
                    <?php endif;?>
                    <input type="submit" name="submitButton" value="Submit" class="goldButton">
                </div>
            </fieldset>
        </form>
        <?php if(isset($_POST['submitButton'])): Register\registerMaster(); header("Refresh:0"); ?>
        <?php endif; ?>
    </div>