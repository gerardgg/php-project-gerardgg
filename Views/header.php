<?php

// Provides access to namespaces to work properly
require_once 'Logic/generics.php';

\session_start();
 if(!isset($_SESSION['userInfo']['role'])) {
    $_SESSION['userInfo']['role'] = 'visitor';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS Stylesheets -->
    <link rel="stylesheet" href="./Styles/normalize.css">
    <link rel="stylesheet" href="./Styles/styles.css">
    <!-- Fonts  -->
    <link href="https://fonts.googleapis.com/css2?family=Cormorant:wght@300;500;700&display=swap" rel="stylesheet"> 
    <title>Restaurant - gerardgg</title>
</head>
<body>
    <div id="wrapper">
        <header>
            <?php require_once 'Views/menu.php'; ?>
        </header>
        <main>
            <div id="mainContent">