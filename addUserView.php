<?php // Provides access to view ?>
<?php require_once 'Views/header.php'; ?>
<?php // Verify permissions ?>
<?php require_once 'Views/assets/permissions/verifyAdmin.php' ?>
<?php // Provides access to namespaces to work properly ?>
<?php require_once 'Logic/administration.php'; ?>
<?php // Needed namespaces for required functions or constants ?>
<?php use GoldHotel\Administration\Users as admUsers; ?>
<h1>Add user</h1>
<?php require_once 'Views/registerForm.php' ?>
<?php // Provides access to view ?>
<?php require_once 'Views/footer.php'; ?>