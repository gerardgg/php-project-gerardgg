<?php

/* Logic about generating a administrable output for menu */
namespace GoldHotel\Administration\Menu {

    // Provides access to namespaces to work properly
    require_once 'Logic/products.php';
    require_once 'Logic/users.php';
    // Needed namespaces for required functions or constants
    use GoldHotel\Products\Generics as ProductGenerics;
    use GoldHotel\Products\Menu as Menu;

    /** masterEditableMenu: Function that holds the flux for generate an output administrable table for menu products.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {string} $menu: outputable menu.
     */
    function masterEditableMenu() {
        $menuData = ProductGenerics\printeableMenuData(Menu\MENUFILE);
        if(count($menuData) != 0) {
            $menu = printEditableMenu($menuData);
            return $menu;
        }
        
    }

    /** printEditableMenu: Function that generates an HTML table from an array.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {string} $output: outputable products inside a table.
     */
    function printEditableMenu(array $orderedMenu): string {
        $output = '<div class=\'menu\'>';
        foreach ($orderedMenu as $categoryName => $products) {
            $output .= '<table class=\'menuCategoryTable\'><caption>'.ucfirst($categoryName).'</caption>';
            $output .= '<tr><th>Name</th><th>Price</th><th>Edit</th></tr>';
            foreach ($products as $product) {
                $output .= '<tr><td>'.ucwords($product[2]).'</td>';
                $output .= '<td class=\'price\'>'.ucwords($product[3]).' €‎</td>';
                $output .= "<td class=\"edit\"><form action=\"editMenu.php\" method=\"POST\">".
                "<input type=\"hidden\" name=\"productId\" value=\"{$product[0]}\"></input>".
                "<button type=\"submit\" name=\"submitEdit\" value=\"product\" class=\"goldButton noMargin\">Edit</button>".
                "<button type=\"submit\" formaction=\"deleteProduct.php\" name=\"submitEdit\" value=\"delete\" class=\"goldButton redBg noMargin\">Delete</button>".
                "</td></form>";
            }
            $output .= '</table>';
        }
        $output .= '</div>';
        return $output;
    }
}
/******************************************************************************/

/* Logic about generating a administrable output for users */
namespace GoldHotel\Administration\Users {
    // Provides access to namespaces to work properly
    require_once 'Logic/users.php';
    // Needed namespaces for required functions or constants
    use GoldHotel\Users\Generics as UserGenerics;

    /** masterEditableUsers: Function that holds the flux for generate an output administrable table for users.
     * @author gerardgg
     * @version 1.0
     * @date 23/10/2020
     * @return {string} $users: outputable users.
     */
    function masterEditableUsers() {
        $usersData = UserGenerics\getUsersList();
        if(\count($usersData) != 0) {
            echo printEditableUsers($usersData);
        }
    }

    /** printEditableUsers: Function that generates an HTML table from an array.
     * @author gerardgg
     * @version 1.0
     * @date 23/10/2020
     * @param {array} $users: array with users.
     * @return {string} $output: outputable table with users.
     */
    function printEditableUsers($users) {
        $output = '<div class=\'menu\'>';
        $output .= '<table class=\'menuCategoryTable\'><caption>User list</caption>';
        $output .= '<tr><th>Username</th><th>Password</th><th>Role</th><th>Name</th><th>Surname</th><th>Edit</th></tr>';
        foreach ($users as $user) {
            $output .= '<tr><td>'.$user[0].'</td><td>Encrypted</td><td>'.$user[2].'</td><td>'.$user[3].'</td><td>'.$user[4].'</td><td class="maxContent dpBlock">'.
            '<form action="editUserView.php" method="POST">'.
            '<input type="hidden" name="username" value="'.$user[0].'">'.
            '<button name="editUser" value="editUser" class="goldButton noMargin">Edit</button>'.
            '<button name="deleteUser" value="delete" class="goldButton redBg noMargin" formaction="deleteUser.php">Delete</button>'.
            '</form></td></tr>';
        }
        $output .= '</table>';
        $output .= '</div>';
        return $output;
    }

}