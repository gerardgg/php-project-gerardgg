<?php

/* Logic about generic edit funcionalities. This namespaces holds functions that can be used for edit a product or a user*/
namespace GoldHotel\Edit\Generics {

    // Provides access to namespaces to work properly
    require_once 'Logic/products.php';
    require_once 'Logic/files.php';
    require_once 'Logic/users.php';
    // Needed namespaces for required functions or constants
    use GoldHotel\Generics as GlobalGenerics;
    use GoldHotel\Products\Generics as ProductGenerics;
    use GoldHotel\Products\Menu as Menu;
    use GoldHotel\Files as Files;
    use GoldHotel\Users\Generics as UserGenerics;

    ########################
    #### MENU FUNCTIONS ####
    ########################

    /** checkDataIsNotEmpty: Function that verifies that variables are not null.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @param {array} $dataArray: array wiith data.
     * @return {bool} false if they are not empty otherwise true.
     */
    function checkDataIsNotEmpty(array $dataArray): bool {
        foreach ($dataArray as $dataElem) {
            if(empty($dataElem)) {
                return true;
            }
        }
        return false;
    }

    /** getMenuProductsData: Function that gets products data.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {array} $parsedMenuData: with products data.
     */
    function getMenuProductsData(): array {
        $rawMenuData = ProductGenerics\getRawMenusData(Menu\MENUFILE);
        if(\count($rawMenuData) > 0) {
            $parsedMenuData = ProductGenerics\parseMenuFileData($rawMenuData);
            return $parsedMenuData;
        } else {
            \header('Location: adminMenu.php');
        }  
    }

    /** getCategories: Function that gets category names.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {array} $categories: with category names.
     */
    function getCategories(): array {
        $categories = ProductGenerics\getCategories();
        if(\count($categories) > 0) {
            return $categories;
        } else {
            \header('Location: adminMenu.php');
        }
    }

    /** outputableCategoriesOptions: Function parses array and concatenates each element inside an option tag.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @param {array} $categoriesArray: array of categories names.
     * @return {string} $categories: String with all categories inside option tag.
     */
    function outputableCategoriesOptions(array $categoriesArray): string {
        $categories = "";
        foreach ($categoriesArray as $category) {
            $categories .= '<option value="'.$category.'">'.\ucfirst($category).'</option>';
        }
        return $categories;
    }

    /** prepareDataToSave: Function generates a string prepared to be saved with the new data.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @param {array} $fileData: array of menu products.
     * @param {array} $newTargetData: user introduced updated data.
     * @param {array} $targetProductData: array with old data and index to modify by default null.
     * @return {string} $result: String with all products prepared with format to be saved.
     */
    function prepareDataToSave(array $fileData, array $newTargetData = null, array $targetProductData = null, bool $users = false): string {
        if(!$users) {
            if($newTargetData != null) {
                unset($newTargetData['confirmEdit']);
                $newTargetData['productName'] = \strtolower($newTargetData['productName']);
            }
            if($targetProductData != null) {
                $fileData[$targetProductData['index']] = $newTargetData;
            } else if ($newTargetData != null){
                $id = $fileData[\array_key_last($fileData)][0] + 1;
                $newTargetData = array('id' => $id) + $newTargetData;
                \array_push($fileData, $newTargetData);
            }
            foreach ($fileData as $key => $product) {
                $fileData[$key] = $product = \implode(';', $product);
            }
            $result = \implode(PHP_EOL, $fileData);
            return $result;
        } else {
            unset($newTargetData['confirmEdit']);
            foreach ($fileData as $user => $value) {
                if($value[0] == $newTargetData['username']) {
                    $fileData[$user] = $newTargetData;
                    
                }
                $fileData[$user] = \implode(';', $fileData[$user]);
            }
            $result = \implode(\PHP_EOL, $fileData);
            return $result;
        }
    }

    /** verifyDataForm: Filters user input data.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {array} $editData: filtered user input data.
     */
    function verifyDataForm(): array {
        $editData = array();
        $editType = verifyIsModifying();
        \array_push($editData, $editType);
        $productId = $_POST['productId'] ? (int) \filter_input(INPUT_POST, 'productId', FILTER_SANITIZE_STRING) : null;
        \array_push($editData, $productId);
        return $editData;
    }

    #########################
    #### USERS FUNCTIONS ####
    #########################

    /** getUsersData: Function that gets users data.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {array} $parsedMenuData: with users data.
     */
    function getUsersData(): array {
        $usersData = UserGenerics\getUsersList();
        if(\count($usersData) > 0) {
            return $usersData;
        } else {
            \header('Location: adminMenu.php');
        }  
    }

    /** verifyDataUsersForm: Filters user input data.
     * @author gerardgg
     * @version 1.0
     * @date 23/10/2020
     * @return {string} $username: Filtered user input data.
     */
    function verifyDataUsersForm() {
        $username = \filter_input(INPUT_POST, 'username');
        return $username;
    }

    /** formatRoles: Function that generates a output with users info and edit/delete buttons.
     * @author gerardgg
     * @version 1.0
     * @date 23/10/2020
     * @param {array} $roles: array with user roles:
     * @param {string} $userRole: string with target user role:
     * @return {string} $outputableRoles: Output of roles within option tags.
     */
    function formatRoles(array $roles, string $userRole) {
        $output = '';
        foreach ($roles as $role) {
            if($role == $userRole) {
                $output .= '<option value="'.$userRole.'" selected>'.$userRole.'</optipon>';
            } else {
                $output .= '<option value="'.$role.'">'.$role.'</optipon>';
            }
        }
        return $output;
    }

    ################################
    #### MENU / USERS FUNCTIONS ####
    ################################

    /** findEntity: Function that verifies that variables are not null.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @param {int} $targetId: integer number equivalent to an id
     * @param {array} $parsedDataFile: array with the lines of data file ready to be explored
     * @return {bool} false if they are not empty otherwise true.
     */
    function findEntity(string $targetId, array $parsedDataFile): array {
        $result = array();
        $counter = 0;
        foreach ($parsedDataFile as $entity) {
            if($entity[0] == $targetId) {
                $foundEntity = $entity;
                $result['entity'] = $foundEntity;
                $result['index'] = $counter;
                return $result;
            }
            $counter++;
        }
        return array('targetNotFound');
    }

    /** verifyIsDeleting: Function that verifies if delete parameter was defined.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @param {string} $productData: data from the form.
     * @return {bool} true if is deleting otherwise false
     * */
    function verifyIsDeleting(string $deleteFlag): bool {
        if($deleteFlag == 'delete') {
            return true;
        } else {
            return false;
        }
    }

    /** verifyIsModifying: Function that verifies that submit button value is defined in $_POST array.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {string} $editType: value of submit button index of $_POST.
     */
    function verifyIsModifying(): string {
        if(isset($_POST['submitEdit'])) {
            $editType = $_POST['submitEdit'] ? \filter_input(INPUT_POST, 'submitEdit', FILTER_SANITIZE_STRING) : null;
            return $editType;
        } else {
            $_SESSION['editError'] = GlobalGenerics\FORMEDEDITFORMERROR;
            \header('Location: adminMenu.php');
        }
    }

    /** saveData: Function that saves a string to a file.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @param {string} $dataFilename: filename to write
     * @param {string} $readyToSaveData: arra
     */
    function saveData(string $dataFilename, string $readyToSaveData) {
        switch($dataFilename) {
            case 'database/menu.info':
                $redirect = 'adminMenu.php';
            break;
            case 'database/users.info':
                $redirect = 'adminUsers.php';
            break;
        }
        $fileExists = Files\checkIfFileExists($dataFilename);
        if($fileExists) {
            $openedFile = Files\openFileWritePermission($dataFilename);
            if($openedFile) {
                \ftruncate($openedFile, 0);
                $writeResult = Files\writeLine($openedFile, $readyToSaveData);
                if($writeResult) {
                    $_SESSION['editSuccessful'] = GlobalGenerics\FORMEDEDITSUCCESSFUL;
                    \header("Location: $redirect");
                } else {
                    $_SESSION['dbError']['dbWriteError'] = GlobalGenerics\FORMEDEDDBQUERYERROR;
                    \header("Location: $redirect");
                }
            } else {
                $_SESSION['dbError']['dbErrorNotPerms'] = GlobalGenerics\FORMEDEDDBQUERYERROR;
                \header("Location: $redirect");
            }
        } else {
            $_SESSION['dbError']['dbErrorNotFound'] = GlobalGenerics\FORMEDEDDBCONNECTIONERROR;
            \header("Location: $redirect");
        }
    }

}
/******************************************************************************/

/* Logic about perfom product modifications funcionalities like: save user introduced changes...*/
namespace GoldHotel\Edit\Modificator\Products {

    // Provides access to namespaces to work properly
    require_once 'Logic/products.php';
    // Needed namespaces for required functions or constants
    use GoldHotel\Products\Menu as Menu;
    use GoldHotel\Edit\Generics as EditGenerics;
    use GoldHotel\Generics as GlobalGenerics;

    /** masterSaveChanges: Function that holds all the flux to perfom a product data change.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     */
    function masterSaveChanges() {
        $newProductData = verifyDataForm();
        if($newProductData[\array_key_last($newProductData)] == 'editProduct') {
            $areEmptyFields = EditGenerics\checkDataIsNotEmpty($newProductData);
            if(!$areEmptyFields) {
                $menuData = EditGenerics\getMenuProductsData();
                $foundData = EditGenerics\findEntity($newProductData['id'], $menuData);
                if(!isset($foundData['targetNotFound'])) {
                    $readyToSaveData = EditGenerics\prepareDataToSave($menuData, $newProductData, $foundData);
                    EditGenerics\saveData(Menu\MENUFILE, $readyToSaveData);
                } else {
                    $_SESSION['editErrorProductNotFound'] = GlobalGenerics\FORMEDEDPRODUCTNOTFOUNDERROR;
                    \header('Location: adminMenu.php');
                }
            } else {
                $_SESSION['editError'] = GlobalGenerics\FORMEDEMPTYFIELDSERROR;
                \header('Location: adminMenu.php');
            }
        } else {
            $_SESSION['editError'] = GlobalGenerics\FORMEDEDITFORMERROR;
            \header('Location: adminMenu.php');
        }
    }

    /** verifyDataForm: Function that filters user introduced data.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {array} $filteredUserInput: filtered data.
     */
    function verifyDataForm(): array {
        $filterTemplate = array(
            'id' => FILTER_VALIDATE_INT,
            'productCategories' => FILTER_SANITIZE_STRING,
            'productName' => FILTER_SANITIZE_STRING,
            'price' => FILTER_VALIDATE_FLOAT,
            'confirmEdit' => FILTER_SANITIZE_STRING
        );
        $filteredUserInput = \filter_input_array(INPUT_POST, $filterTemplate);
        return $filteredUserInput;
    }
}
/******************************************************************************/

/* Logic about generate an output form to edit a product */
namespace GoldHotel\Edit\Views\Products {

    // Needed namespaces for required functions or constants
    use GoldHotel\Generics as GlobalGenerics;
    use GoldHotel\Edit\Generics as EditGenerics;

    /** loadTargeEditProductData: Function that gets product data, searches the product and prepares and output form to edit a product.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {string} $editOutput: Form with product fields and target product data otherwise opens a session variable with error info  and redirect to adminMenu.php and displays an error..
     */
    function loadTargeEditProductData(): string {
        $editData = verifyDataForm();
        $areEmptyFields = EditGenerics\checkDataIsNotEmpty($editData);
        if(!$areEmptyFields && $editData[0] == "product") {
            $menuData = EditGenerics\getMenuProductsData();
            $targetData = EditGenerics\findEntity($editData[1], $menuData);
            $targetProduct = $targetData['entity'];
            if(!isset($foundData['targetNotFound'])) {
                $getCategories = EditGenerics\getCategories();
                if($getCategories != 0) {
                    $categories = EditGenerics\outputableCategoriesOptions($getCategories);
                    $editOutput = '<h2>Editing product: '.$targetProduct[0].', '.$targetProduct[2].'</h2>'.
                    '<form method="POST" action="editProduct.php"><fieldset>'.
                    '<div><label for="id">ID:</label><input name="id" type="number" readOnly value="'.$targetProduct[0].'"></div>'.
                    '<div><label for="productCategories">Product categories:</label><select name="productCategories">'.$categories.'</select></div>'.
                    '<div><label for="productName">Product name: </label><input name="productName" type="text" value="'.\ucwords($targetProduct[2]).'"></div>'.
                    '<div><label for="pridce">Price</label><input name="price" type="number" value="'.$targetProduct[3].'"></div>'.
                    '</fieldset>'
                    .'<button type="submit" name="confirmEdit" value="editProduct">Submit changes</button></form>';
                    return $editOutput;
                }
            } else {
                $_SESSION['editErrorProductNotFound'] = GlobalGenerics\FORMEDEDPRODUCTNOTFOUNDERROR;
                \header('Location: adminMenu.php');
            }
        } else {
            $_SESSION['editError'] = GlobalGenerics\FORMEDEDITFORMERROR;
            \header('Location: adminMenu.php');
        }
    }

    /** verifyDataForm: Filters user input data.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {array} $editData: filtered user input data.
     */
    function verifyDataForm(): array {
        $editData = array();
        $editType = EditGenerics\verifyIsModifying();
        \array_push($editData, $editType);
        $productId = $_POST['productId'] ? (int) \filter_input(INPUT_POST, 'productId', FILTER_SANITIZE_STRING) : null;
        \array_push($editData, $productId);
        return $editData;
    }
}
/******************************************************************************/

/* Logic about generate an output form to edit a product */
namespace GoldHotel\Edit\Add\Products {

    // Needed namespaces for required functions or constants
    use GoldHotel\Generics as GlobalGenerics;
    use GoldHotel\Edit\Generics as EditGenerics;
    use GoldHotel\Products\Menu as Menu;

    /** masterAddProduct: Function that holds all the flux to add a product.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     */
    function masterAddProduct() {
        $newProductData = verifyDataForm();
        $areEmpty = EditGenerics\checkDataIsNotEmpty($newProductData);
        if(!$areEmpty) {
            if($newProductData['confirmEdit']) {
                $menuData = EditGenerics\getMenuProductsData();
                $readyToSaveData = EditGenerics\prepareDataToSave($menuData, $newProductData);
                EditGenerics\saveData(Menu\MENUFILE, $readyToSaveData);
            } else {
                $_SESSION['editError'] = GlobalGenerics\FORMEDEDITFORMERROR;;
                \header('Location: adminMenu.php');
            }
        } else {
            $_SESSION['editError'] = GlobalGenerics\FORMEDEMPTYFIELDSERROR;
            \header('Location: adminMenu.php');
        }
        
    }

    /** outputCategories: Function that returns list of categories.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {string} $categories: string with categories.
     */
    function outputCategories(): string {
        $getCategories = EditGenerics\getCategories();
        $categories = EditGenerics\outputableCategoriesOptions($getCategories);
        return $categories;
    }

    /** verifyDataForm: Function that filters user input data.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {array} $filteredUserInput: filtered data.
     */
    function verifyDataForm(): array {
        $filterTemplate = array(
            'productCategories' => FILTER_SANITIZE_STRING,
            'productName' => FILTER_SANITIZE_STRING,
            'productPrice' => FILTER_VALIDATE_FLOAT,
            'confirmEdit' => FILTER_SANITIZE_STRING
        );
        $filteredUserInput = \filter_input_array(INPUT_POST, $filterTemplate);
        return $filteredUserInput;
    }
}
/******************************************************************************/

/* Logic about deleting products */
namespace GoldHotel\Edit\Delete\Products {

    // Needed namespaces for required functions or constants
    use GoldHotel\Generics as GlobalGenerics;
    use GoldHotel\Edit\Generics as EditGenerics;
    use GoldHotel\Products\Menu as Menu;

    /** masterDeleteProduct: Function that holds all the flux for product deletion.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * */
    function masterDeleteProduct() {
        $targetProductData = EditGenerics\verifyDataForm();
        $areEmpty = EditGenerics\checkDataIsNotEmpty($targetProductData);
        if(!$areEmpty) {
            $deleteFlag = EditGenerics\verifyIsDeleting($targetProductData[0]);
            if($deleteFlag) {
                $menuData = EditGenerics\getMenuProductsData();
                for ($i=0; $i < \count($menuData); $i++) { 
                    if($menuData[$i][0] == $targetProductData[1]) {
                        unset($menuData[$i]);
                        break;
                    }
                }
                $readyToSaveData = EditGenerics\prepareDataToSave($menuData);
                EditGenerics\saveData(Menu\MENUFILE, $readyToSaveData);
            } else {
                $_SESSION['editError'] = GlobalGenerics\FORMEDEDITFORMERROR;
                \header('Location: adminMenu.php');
            }
        } else {
            $_SESSION['editError'] = GlobalGenerics\FORMEDEMPTYFIELDSERROR;
            \header('Location: adminMenu.php');
        }
        
    }
}
/******************************************************************************/

/* Logic about generate an output form to edit a product */
namespace GoldHotel\Edit\Views\Users {

    // Needed namespaces for required functions or constants
    use GoldHotel\Generics as GlobalGenerics;
    use GoldHotel\Edit\Generics as EditGenerics;
    use GoldHotel\Users\Generics as UserGenerics;

    /** loadTargeEditProductData: Function that generates a output with users info and edit/delete buttons.
     * @author gerardgg
     * @version 1.0
     * @date 23/10/2020
     * @return {string} $editOutput: Output with forms of users info.
     */
    function loadTargeEditUserData(): string {
        $editData = EditGenerics\verifyDataUsersForm();
        if(!empty($editData)) {
            $usersData = EditGenerics\getUsersData();
            $targetUser = EditGenerics\findEntity($editData, $usersData);
            if(!isset($targetUser['targetNotFound'])) {
                $getRoles = UserGenerics\getRoles();
                if(count($getRoles) != 0) {
                    $outputableRoles = EditGenerics\formatRoles($getRoles, $targetUser['entity'][2]);
                    $editOutput = '<h2>Editing product: '.$targetUser['entity'][0].'</h2>'.
                    '<form method="POST" action="editUser.php"><fieldset>'.
                    '<div><label for="username">Username:</label><input name="username" type="username" readOnly value="'.$targetUser['entity'][0].'"></div>'.
                    '<div><label for="password">Password: </label><input name="password" type="password" value="'.$targetUser['entity'][1].'"></div>'.
                    '<div><label for="role">Role:</label><select name="role">'.$outputableRoles.'</select></div>'.
                    '<div><label for="name">Name: </label><input name="name" type="text" value="'.$targetUser['entity'][3].'"></div>'.
                    '<div><label for="surname">Surname: </label><input name="surname" type="text" value="'.$targetUser['entity'][4].'"></div>'.
                    '</fieldset>'
                    .'<button type="submit" name="confirmEdit" value="userEdit">Submit changes</button></form>';
                    return $editOutput;
                }
            } else {
                $_SESSION['editErrorProductNotFound'] = GlobalGenerics\FORMEDEDPRODUCTNOTFOUNDERROR;
                header('Location: adminUsers.php');
            }
        } else {
            $_SESSION['editError'] = GlobalGenerics\FORMEDEDITFORMERROR;
            header('Location: adminUsers.php');
        }
    }

    /** loadTargeEditForm: Function that generates a output with users info and edit/delete buttons.
     * @author gerardgg
     * @version 1.0
     * @date 23/10/2020
     * @param {array} $userData:
     * @return {string} $editOutput: Output with forms of users info.
     */
    function selectTargetUser(array $usersData, int $username): array {
        foreach ($usersData as $user) {
            if($user[0] == $username) {
                return $user;
            } else {

            }
        }
    }
}
/******************************************************************************/

/* Logic about perfom product modifications funcionalities like: save user introduced changes...*/
namespace GoldHotel\Edit\Modificator\Users {

    // Provides access to namespaces to work properly
    require_once 'Logic/users.php';
    // Needed namespaces for required functions or constants
    use GoldHotel\Users\Generics as UserGenerics;
    use GoldHotel\Edit\Generics as EditGenerics;
    use GoldHotel\Generics as GlobalGenerics;

    /** masterSaveChanges: Function that holds all the flux to perfom a product data change.
     * @author gerardgg
     * @version 1.0
     * @date 23/10/2020
     */
    function masterSaveChanges() {
        $newUserData = verifyDataForm();
        if($newUserData[array_key_last($newUserData)] == 'userEdit') {
            $areEmptyFields = EditGenerics\checkDataIsNotEmpty($newUserData);
            if(!$areEmptyFields) {
                $usersData = EditGenerics\getUsersData();
                $newUserOldData = EditGenerics\findEntity($newUserData['username'], $usersData);
                if(!isset($newUserOldData['targetNotFound'])) {
                    $readyToSaveData = EditGenerics\prepareDataToSave($usersData, $newUserData, null, true);
                    EditGenerics\saveData(UserGenerics\USERSFILE, $readyToSaveData);
                    die();
                } else {
                    $_SESSION['editErrorProductNotFound'] = GlobalGenerics\FORMEDEDPRODUCTNOTFOUNDERROR;
                    header('Location: adminUsers.php');
                }
            } else {
                $_SESSION['editError'] = GlobalGenerics\FORMEDEMPTYFIELDSERROR;
                header('Location: adminUsers.php');
            }
        } else {
            $_SESSION['editError'] = GlobalGenerics\FORMEDEDITFORMERROR;
            header('Location: adminUsers.php');
        }
    }

    /** verifyDataForm: Function that filters user introduced data.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {array} $filteredUserInput: filtered data.
     */
    function verifyDataForm(): array {
        $filterTemplate = array(
            'username' => FILTER_SANITIZE_STRING,
            'password' => FILTER_SANITIZE_STRING,
            'role' => FILTER_SANITIZE_STRING,
            'name' => FILTER_SANITIZE_STRING,
            'surname' => FILTER_SANITIZE_STRING,
            'confirmEdit' => FILTER_SANITIZE_STRING
        );
        $filteredUserInput = filter_input_array(INPUT_POST, $filterTemplate);
        return $filteredUserInput;
    }
}
/******************************************************************************/

/* Logic about deleting products */
namespace GoldHotel\Edit\Delete\Users {

    // Needed namespaces for required functions or constants
    use GoldHotel\Generics as GlobalGenerics;
    use GoldHotel\Edit\Generics as EditGenerics;
    use GoldHotel\Users\Generics as UserGenerics;

    /** masterDeleteUser: Function that holds all the flux for user deletion.
     * @author gerardgg
     * @version 1.0
     * @date 24/10/2020
     * */
    function masterDeleteUser() {
        $targetUserData = verifyDataForm();
        if(!empty($targetUserData)) {
            $deleteFlag = EditGenerics\verifyIsDeleting($targetUserData['deleteUser']);
            if($deleteFlag) {
                $usersData = EditGenerics\getUsersData();
                $newUsersDataFile = deleteUser($targetUserData['username'], $usersData);
                if(!empty($newUsersDataFile)) {
                    EditGenerics\saveData(UserGenerics\USERSFILE, $newUsersDataFile);
                } else {
                    $_SESSION['editError'] = GlobalGenerics\FORMEDUSERNOTFOUNDERROR;
                    header('Location: adminUsers.php');
                }
            } else {
                $_SESSION['editError'] = GlobalGenerics\FORMEDEDITFORMERROR;
                header('Location: adminUsers.php');
            }
        } else {
            $_SESSION['editError'] = GlobalGenerics\FORMEDEMPTYFIELDSERROR;
            header('Location: adminUsers.php');
        }
        
    }

    /** deleteUser: Function that generates a string with users except delete target.
     * @author gerardgg
     * @version 1.0
     * @date 24/10/2020
     * @param {string} $username: delete target username.
     * @param {array} $usersFileContents: array with users.
     * @return {string} $usersFileContents: string with users fields separated by semicolons and new line each other. 
     * */
    function deleteUser(string $username, array $usersFileContents): string {
        foreach ($usersFileContents as $user => $value) {
            if($value[0] == $username) {
                unset($usersFileContents[$user]);
            } else {
                $usersFileContents[$user] = implode(';', $value);
            }
        }
        $usersFileContents = implode(PHP_EOL, $usersFileContents);
        return $usersFileContents;
    }

    /** verifyDataForm: Function that filters user input data.
     * @author gerardgg
     * @version 1.0
     * @date 24/10/2020
     * @return {array} $filteredUserInput: filter user introduced data. 
     * */
    function verifyDataForm(): array {
        $filterTemplate = array(
            'username' => FILTER_SANITIZE_STRING,
            'deleteUser' => FILTER_SANITIZE_STRING
        );

        $filteredUserInput = filter_input_array(INPUT_POST, $filterTemplate);
        return $filteredUserInput;
    }

}
/******************************************************************************/

/* Logic about generate an output form to edit a product */
namespace GoldHotel\Edit\Add\Users {

    // Needed namespaces for required functions or constants
    use GoldHotel\Generics as GlobalGenerics;
    use GoldHotel\Edit\Generics as EditGenerics;
    use GoldHotel\Users\Generics as UserGenerics;

    /** masterAddUser: Function that holds all the flux to add a user.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     */
    function masterAddUser() {
        $newUserData = verifyDataForm();
        $areEmpty = EditGenerics\checkDataIsNotEmpty($newUserData);
        if(!$areEmpty) {
            if($newUserData['confirmEdit']) {
                $getRoles = UserGenerics\getRoles();
                if(count($getRoles) != 0) {
                    $outputableRoles = EditGenerics\formatRoles($getRoles, $targetUser['entity'][2]);
                    $menuData = EditGenerics\getMenuProductsData();
                    $readyToSaveData = EditGenerics\prepareDataToSave($menuData, $newUserData);
                    EditGenerics\saveData(Menu\MENUFILE, $readyToSaveData);
                }
            } else {
                $_SESSION['editError'] = GlobalGenerics\FORMEDEDITFORMERROR;;
                header('Location: adminMenu.php');
            }
        } else {
            $_SESSION['editError'] = GlobalGenerics\FORMEDEMPTYFIELDSERROR;
            header('Location: adminMenu.php');
        }
        
    }

    /** verifyDataForm: Function that filters user input data.
     * @author gerardgg
     * @version 1.0
     * @date 22/10/2020
     * @return {array} $filteredUserInput: filtered data.
     */
    function verifyDataForm(): array {
        $filterTemplate = array(
            'productCategories' => FILTER_SANITIZE_STRING,
            'productName' => FILTER_SANITIZE_STRING,
            'productPrice' => FILTER_VALIDATE_FLOAT,
            'confirmEdit' => FILTER_SANITIZE_STRING
        );
        $filteredUserInput = filter_input_array(INPUT_POST, $filterTemplate);
        return $filteredUserInput;
    }
}

// EOF