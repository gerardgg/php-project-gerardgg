<?php

/* Generic logic about users and his funcionalities like: get user list... */
namespace GoldHotel\Users\Generics {

    // Provides access to namespaces to work properly
    require_once 'Logic/generics.php';
    require_once 'files.php';
    // Needed namespaces for required functions or constants
    use GoldHotel\Generics as GlobalGenerics;
    use GoldHotel\Files as Files;

    // Constant values
    define(__NAMESPACE__.'\USERSFILE', 'database/users.info');
    define(__NAMESPACE__.'\ROLESFILE', 'database/roles.info');

    /** getRoles: Function that gets all roles
     * @author gerardgg
     * @version 1.0
     * @date 06/10/2020 
     * @return {array} $roles: string array with rolenames otherwise empty array.
     */
    function getRoles(): array {
        $fileFound = Files\checkIfFileExists(ROLESFILE);
        if($fileFound) {
            $openedFile = Files\openFileReadPermision(ROLESFILE);
            if($openedFile) {
                $fileLines = Files\getFileLinesToArray($openedFile);
                Files\closeFile($openedFile);
                return $fileLines;
            }
        }
        return array();

    }

     /** findUsersFile: Function that finds the users file
     * @author gerardgg
     * @version 1.0
     * @date 06/10/2020 
     * @return {bool} $fileFound: true if the file is found otherwise false.
     */
    function findUsersFile(): bool {
        $fileFound = Files\checkIfFileExists(USERSFILE);
        return $fileFound;
    }
    
    /** openUsersFileReadOnly: Function that opens the users file with read permission.
     * @author gerardgg
     * @version 1.0
     * @date 06/10/2020 
     * @return {resource} $openedFileResource: resource if the file is found otherwise false.
     */
    function openUsersFileReadOnly() {
        $openedFileResource = Files\openFileReadPermision(USERSFILE);
        return $openedFileResource;
    }

    /** openUsersFileWriteOnly: Function that opens the users file with write permission.
     * @author gerardgg
     * @version 1.0
     * @date 07/10/2020 
     * @return {resource} $openedFileResource: resource if the file is found otherwise false.
     */
    function openUsersFileWriteOnly() {
        $openedFileResource = Files\openFileWritePermission(USERSFILE);
        return $openedFileResource;
    }

    /** closeUsersFile: Function that close users file resource.
     * @author gerardgg
     * @version 1.0
     * @date 7/10/20
     * @param {resource} $openedFileResource: opened resource of users file.
     */
    function closeUsersFile($openedFileResource) {
        Files\closeFile($openedFileResource);
    }

    /** checkEmptyFields: Function that verifies if fields are empty
     * @author gerardgg
     * @version 1.0
     * @date 06/10/2020 
     * @return {bool} $errorFlag: true if there is one or more null fields otherwise false.
     */
    function checkEmptyFields(): bool {
        $errorFlag = false;
        foreach ($_SESSION['formResults'] as $key => $value) {
            if(\is_null($value)) {
                $errorFlag = true;
                return $errorFlag ;
            break;
            }
        }
        return $errorFlag ;
    }
    
    /** getRawUserListArray: Function that saves users file lines into array
     * @author gerardgg
     * @version 1.0
     * @date 06/10/2020 
     * @return {array} $userList: array with user list.
     */
    function getRawUserListArray($openedFileResource): array {
        $userList = Files\getFileLinesToArray($openedFileResource);
        return $userList;
    }

    /** parseUserListArray: Function that separates users fields within array
     * @author gerardgg
     * @version 1.0
     * @date 06/10/2020
     * @param {array} array of the raw users list.
     * @return {array} $userParsedList: array with user list.
     */
    function parseUserListArray(array $userList): array {
        $parsedUserList = array();
        foreach ($userList as $user) {
            $parsedUser = \explode(';', $user);
            \array_push($parsedUserList, $parsedUser);
        }
        return $parsedUserList;
    }

    
    /** addNewUserToUsersFile: Function that writes and saves a new user to users file.
     * @author gerardgg
     * @version 1.0
     * @date 07/10/2020 
     */
    function addNewUserToUsersFile($stringToWrite) {
        $fileFound = findUsersFile();
        if($fileFound) {
            $openedFileResource = openUsersFileWriteOnly($fileFound);
            if($openedFileResource && \is_resource($openedFileResource)) {
                Files\writeLine($openedFileResource, $stringToWrite);
                closeUsersFile($openedFileResource);
            } else {
                closeUsersFile($openedFileResource);
                $_SESSION['errorLogin']['errorUsersFile'] = GlobalGenerics\ERRORHEADER.'with the DB query'.GlobalGenerics\ERROREND;
            }
        } else {
            $_SESSION['errorLogin']['errorUsersFile'] = GlobalGenerics\ERRORHEADER.'with the DB connection'.GlobalGenerics\ERROREND;
        }
    }

    /** getUsersList: Function that gets and return the users list ready to use it.
     * @author gerardgg
     * @version 1.0
     * @date 06/10/2020 
     * @return {array} $userParsedList: array with user list ready to use otherwise an empty array.
     */
    function getUsersList(): array {
        $fileFound = findUsersFile();
        if($fileFound) {
            $openedFileResource = openUsersFileReadOnly();
            if($openedFileResource && \is_resource($openedFileResource)) {
                $rawUserList = getRawUserListArray($openedFileResource);
                $readyUserList = parseUserListArray($rawUserList);
                closeUsersFile($openedFileResource);
                return $readyUserList;
            } else {
                closeUsersFile($openedFileResource);
                $_SESSION['errorLogin']['errorUsersFile'] = GlobalGenerics\ERRORHEADER.'with the DB query'.GlobalGenerics\ERROREND;
                return array('error' => true);
            }
        } else {
            $_SESSION['errorLogin']['errorUsersFile'] = GlobalGenerics\ERRORHEADER.'with the DB connection'.GlobalGenerics\ERROREND;
            return array('error' => true);
        }
    }

    /** destroyUserSession: Function that destroys user session
     * @author gerardgg
     * @version 1.0
     * @date 08/10/2020 
     * @return {bool} true if no sessions are detected, otherwise false.
     */
    function destroyUserSession(): bool {
        if(isset($_SESSION['userInfo'])) {
            unset($_SESSION['userInfo']);
        }
        GlobalGenerics\destroySession();
        if(!\session_id()) {
            return true;
        } else {
            return false;
        }
    }
}
/******************************************************************************/

/* Logic about users and his funcionalities like: login, logout... */
namespace GoldHotel\Users\LogIn {

    // Needed namespaces for required functions or constants
    use GoldHotel\Users\Generics as UserGenerics;

    /** logInMaster: Function that holds the flux of the login process.
     * @author gerardgg
     * @version 1.0
     * @date 05/10/2020 
     */
    function logInMaster() {
        verifyLogInData();
        $emptyFieldsFlag = UserGenerics\checkEmptyFields();
        if(!$emptyFieldsFlag) {
            $userList = UserGenerics\getUsersList();
            if(!isset($userList['error'])) {
                tryLogin($userList);
            }
        }    
    }

    /** verifyLogInData: Function that sanitizes and validates user introduced data in login form
     * @author gerardgg
     * @version 1.0
     * @date 05/10/2020 
     */
    function verifyLogInData() {
        $_SESSION['formResults'] = array();
        $_SESSION['formResults']['username'] = $_POST['username'] ? \filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING) : null;
        $_SESSION['formResults']['password'] = $_POST['password'] ? \filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING) : null;
    }  

    /** tryLogin: Function that tries to login with introduced credentials.
     * @author gerardgg
     * @version 1.0
     * @date 06/10/2020
     * @param {bool} return used to exit function.
     */
    function tryLogin(array $userList) {
        foreach ($userList as $user) {
            if($user[0] == $_SESSION['formResults']['username']) {
                if($user[1] == $_SESSION['formResults']['password']) {
                    $_SESSION['userInfo'] = array();
                    $_SESSION['userInfo']['username'] = $user[0];
                    $_SESSION['userInfo']['role'] = $user[2];
                    $_SESSION['userInfo']['name'] = $user[3];
                    $_SESSION['userInfo']['surname'] = $user[4];
                    return true;
                }
            }
        }
        $_SESSION['invalidCredentials'] = 'Invalid credentials';
        return false;
    }

}
/******************************************************************************/

/* Logic about loging out a user... */
namespace GoldHotel\Users\LogOut {

    // Nedded namespaces for required functions or constants
    use GoldHotel\Users\Generics as UserGenerics;

    /** logOut: Function that logs out a user.
     * @author gerardgg
     * @version 1.0
     * @date 08/10/2020
     * @return {bool} if the user log outs correctly return true otherwise false
     */
    function logOut(): bool {
        $result = UserGenerics\destroyUserSession();
        if($result) {
            return true;
        } else {
            return false;
        }
    }

}
/******************************************************************************/

/* Logic about registering a new user in website... */
namespace GoldHotel\Users\Register {

    // Needed namespaces for required functions or constants
    use GoldHotel\Users\Generics as UserGenerics;

    /** registerMaster: Function that holds the flux of register process.
     * @author gerardgg
     * @version 1.0
     * @date 07/10/2020
     */
    function registerMaster() {
        verifyRegisterData();
        $emptyFieldsFlag = UserGenerics\checkEmptyFields();
        if(!$emptyFieldsFlag) {
            $userList = UserGenerics\getUsersList();
            if(!isset($userList['error'])) {
                $isUnique = verifyNewUsernameUniqueness($userList);
                if($isUnique) {
                    $username = $_SESSION['formResults']['username'];
                    $password = $_SESSION['formResults']['password'];
                    $name = $_SESSION['formResults']['name'];
                    $surname = $_SESSION['formResults']['surname'];
                    $userSaveLine = PHP_EOL."$username;$password;registrered;$name;$surname";
                    UserGenerics\addNewUserToUsersFile($userSaveLine);
                    $_SESSION['registerCompleted'] = "User registrered correctly";
                } else {
                    $_SESSION['formResults']['usedUsername'] = 'This username is already used. Please use pick other.';
                }
            }
        }
    }

    /** verifyNewUsernameUniqueness: Function that verifies that the introduced username is not already used.
     * @author gerardgg
     * @version 1.0
     * @date 07/10/2020
     */
    function verifyNewUsernameUniqueness(array $userList): bool {
        $username = $_SESSION['formResults']['username'];
        $uniquenessFlag = true;
        foreach ($userList as $user) {
            if($user[0] == $username) {
                $uniquenessFlag = false;
            break;
            }
        }
        return $uniquenessFlag;
    }

    /** verifyRegisterData: Function that sanitizes user introduced data.
     * @author gerardgg
     * @version 1.0
     * @date 07/10/2020
     */
    function verifyRegisterData() {
        $_SESSION['formResults'] = array();
        $_SESSION['formResults']['username'] = $_POST['username'] ? \filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING) : null;
        $_SESSION['formResults']['password'] = $_POST['password'] ? \filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING) : null;
        $_SESSION['formResults']['name'] = $_POST['name'] ? \filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING) : null;
        $_SESSION['formResults']['surname'] = $_POST['surname'] ? \filter_input(INPUT_POST, 'surname', FILTER_SANITIZE_STRING) : null;
    }
}

// EOF