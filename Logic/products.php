<?php

/* Logic about menus and his common funcionalities like: read product categories... */
namespace GoldHotel\Products\Generics {

    // Provides access to namespaces to work properly
    require_once 'Logic/generics.php';
    require_once 'Logic/files.php';
    // Needed namespaces for required functions or constants 
    use GoldHotel\Generics as GlobalGenerics;
    use GoldHotel\Files as Files;

    // Constant values
    \define(__NAMESPACE__.'\CATEGORIESFILE', 'database/categories.info');

    /** getCategories: Function that reads categories file
     * @author gerardgg
     * @version 1.0
     * @date 03/10/2020 
     * @return {array} $categories: array with category names otherwise an empty array.
     */
    function getCategories(): array {
        $categoriesExists = Files\checkIfFileExists(CATEGORIESFILE);
        if($categoriesExists) {
            $openedCategoriesFile = Files\openFileReadPermision(CATEGORIESFILE);
            if($openedCategoriesFile && \is_resource($openedCategoriesFile)) {
                $categories = Files\getFileLinesToArray($openedCategoriesFile);
                Files\closeFile($openedCategoriesFile);
                return $categories;
            } else {
                $_SESSION['dbError']['dbErrorNotPerms'] = GlobalGenerics\FORMEDEDDBQUERYERROR;
                return array();
            }
        } else {
            $_SESSION['dbError']['dbErrorNotFound'] = GlobalGenerics\FORMEDEDDBCONNECTIONERROR;
            return array();
        }
    }

    /** getRawMenusData: Function that reads a menu file
     * @author gerardgg
     * @version 1.0
     * @date 03/10/2020 
     * @return {array} $rawMenu: array with a menu file lines otherwise an empty array.
     */
    function getRawMenusData(string $filePath): array {
        $menuFileExists = Files\checkIfFileExists($filePath);
        if($menuFileExists) {
            $openedMenuFile = Files\openFileReadPermision($filePath);
            if($openedMenuFile && \is_resource($openedMenuFile)) {
                $rawMenu = Files\getFileLinesToArray($openedMenuFile);
                Files\closeFile($openedMenuFile);
                return $rawMenu;
            } else {
                $_SESSION['dbError']['dbErrorNotPerms'] = GlobalGenerics\FORMEDEDDBQUERYERROR;
                return array();
            }
        } else {
            $_SESSION['dbError']['dbErrorNotFound'] = GlobalGenerics\FORMEDEDDBCONNECTIONERROR;
            return array();
        }
    }

    /** parseMenuFileData: Function that reads the day menu file
     * @author gerardgg
     * @version 1.0
     * @date 03/10/2020
     * @param {array} $rawDayMenu: array with raw readed menu data. 
     * @return {array} $parsedDayMenu: array with parsed menu file lines otherwise false.
     */
    function parseMenuFileData(array $rawMenu) {
        foreach ($rawMenu as $key => $value) {
            $rawMenu[$key] = \explode(';', $value);
            $rawMenu[$key][0] = (int) $rawMenu[$key][0];
        }
        $parsedMenu = $rawMenu;
        return $parsedMenu;
    }

    /** orderMenu: Function that orders a menu file by categories
     * @author gerardgg
     * @version 1.0
     * @date 03/10/2020
     * @param {array} $categories: array with categories 
     * @return {array} $parsedMenu: array with parsed menu file lines otherwise false.
     */
    function orderMenu(array $categories, array $parsedMenu): array {
        $productsByCateogries = \array_flip($categories);
        $categoryNames = \array_keys($productsByCateogries);
        $productsByCateogries = \array_fill(0, count($productsByCateogries), array());
        $productsByCateogries = \array_combine($categoryNames, $productsByCateogries);
        foreach ($categories as $category) {
            foreach ($parsedMenu as $product) {
                if($category == $product[1]) {
                    \array_push($productsByCateogries[$category], $product);
                }
            }
        }
        return $productsByCateogries;
    }

    /** printeableMenuData: Function that makes menu data ready to print
     * @author gerardgg
     * @version 1.0
     * @date 03/10/2020
     * @param {string} $menuFilename: filename to read
     * @return {array} $preparedData: array with parsed menu file lines otherwise empty array.
     */
    function printeableMenuData(string $menuFilename): array {
        $fileContent = getRawMenusData($menuFilename);
        if(\count($fileContent) != 0) {
            $categories = getCategories();
            if(\count($categories) != 0) {
                $parsedMenu = parseMenuFileData($fileContent);
                $orderedMenu = orderMenu($categories, $parsedMenu);
                return $orderedMenu;
            }
        }
        return array();
    }

}
/******************************************************************************/

/* Logic about day menu and his funcionalities like: list products ordered by categories in lists... */
namespace GoldHotel\Products\DayMenu {

    // Needed namespaces for required functions or constants 
    use GoldHotel\Products\Generics as ProductGenerics;

    // Constant values
    define(__NAMESPACE__.'\DAYMENUFILE', 'database/daymenu.info');

    /** masterPrintDayMenu: Function holds the print process of the day menu
     * @author gerardgg
     * @version 1.0
     * @date 03/10/2020 
     */
    function masterPrintDayMenu() {
        $menuData = ProductGenerics\printeableMenuData(DAYMENUFILE);
        if(\count($menuData) != 0) {
            printDayMenu($menuData);
        }
    } 

    /** printDayMenu: Function that prints the final output of day menu
     * @author gerardgg
     * @version 1.0
     * @date 03/10/2020 
     * @param {array} $orderedMenu: array with day menu products
     */
    function printDayMenu(array $orderedDayMenu) {
        $output = '<div class=\'dayMenu\'>';
        foreach ($orderedDayMenu as $categoryName => $products) {
            $output .= '<h2>'.\ucfirst($categoryName).'</h2><ul class=\'dayMenuUl\'>';
            foreach ($products as $product) {
                $output .= '<li class=\'dayMenuLi\'><p>'.\ucwords($product[2]).'</p><img class=\'productImg\'src=\'Images/products/'.$product[2].'.jpg\'></li>';
            }
            $output .= '</ul>';
        }
        $output .= '</div>';
        echo $output;
    }

}
/******************************************************************************/

/* Logic about menu and his funcionalities like: print all the products*/
namespace GoldHotel\Products\Menu {

    // Needed namespaces for required functions or constants 
    use GoldHotel\Products\Generics as ProductGenerics;

    // Constant values
    define(__NAMESPACE__.'\MENUFILE', 'database/menu.info');

    /** masterPrintMenu: Function holds the print process of the day menu
     * @author gerardgg
     * @version 1.0
     * @date 03/10/2020 
     */
    function masterPrintMenu() {
        $menuData = ProductGenerics\printeableMenuData(MENUFILE);
        if(\count($menuData) != 0) {
            printMenu($menuData);
        }
    }

    /** printMenu: Function that prints the final output of menu
     * @author gerardgg
     * @version 1.0
     * @date 03/10/2020 
     * @param {array} $orderedMenu: array with menu products
     */
    function printMenu(array $orderedMenu) {
        $output = '<div class=\'menu\'>';
        foreach ($orderedMenu as $categoryName => $products) {
            $output .= '<table class=\'menuCategoryTable\'><caption>'.\ucfirst($categoryName).'</caption>';
            $output .= '<tr><th>Name</th><th>Price</th></tr>';
            foreach ($products as $product) {
                $output .= '<tr><td>'.\ucwords($product[2]).'</td>';
                $output .= '<td class=\'price\'>'.\ucwords($product[3]).' €‎</td></tr>';
            }
            $output .= '</table>';
        }
        $output .= '</div>';
        echo $output;
    }
    
}