<?php

/* Logic that can be used in multiple sections of the website */
namespace GoldHotel\Generics {

    //Constant values
    \define(__NAMESPACE__.'\ERRORHEADER', 'Error: there is a problem. ');
    \define(__NAMESPACE__.'\ERROREND', '. Please contact with the webmaster.');
    \define(__NAMESPACE__.'\CORRECTTEXT', '<p class="text correctText">');
    \define(__NAMESPACE__.'\ERRORTEXT', '<p class="text errorText">');
    \define(__NAMESPACE__.'\CLOSETEXT', '</p>');
    \define(__NAMESPACE__.'\FLEXDIV', '<div class="flexdiv">');
    \define(__NAMESPACE__.'\CLOSEDIV', '</div>');

    // Correct messages
    \define(__NAMESPACE__.'\EDITSUCCESSFUL', 'The addition/edition was successful');

    // Formed correct messages
    \define(__NAMESPACE__.'\FORMEDEDITSUCCESSFUL', FLEXDIV.CORRECTTEXT.EDITSUCCESSFUL.CLOSETEXT.CLOSEDIV);

    // Error messages
    \define(__NAMESPACE__.'\EDITFORMERROR', 'Please don\'t access edit pages directly, use edit buttons instead');
    \define(__NAMESPACE__.'\PRODUCTNOTFOUNDERROR', 'This product was not found');
    \define(__NAMESPACE__.'\USERNOTFOUNDERROR', 'This user was not found');
    \define(__NAMESPACE__.'\DBQUERYERROR', 'The DB query failed');
    \define(__NAMESPACE__.'\DBCONNECTIONERROR', 'The DB connection failed');
    \define(__NAMESPACE__.'\EMPTYFIELDSERROR', 'Please complete all the fields');
    \define(__NAMESPACE__.'\EMPTYUSERNAME', 'Username is required');
    \define(__NAMESPACE__.'\EMPTYPASSWORD', 'Password is required');
    \define(__NAMESPACE__.'\EMPTYNAME', 'Name is required');
    \define(__NAMESPACE__.'\EMPTYSURNAME', 'Surname is required');
    \define(__NAMESPACE__.'\ONLYVISITORSERROR', 'Only visitors can visit this page');
    \define(__NAMESPACE__.'\ONLYLOGGEDERROR', 'Only logged users can visit this page');
    \define(__NAMESPACE__.'\ONLYAUTHORIZEDERROR', 'Only authorized users can visit this page');

    // Formed error messages
    #define(__NAMESPACE__.'\FORMED', ERRORTEXT.ERRORHEADER..ERROREND.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDEDITFORMERROR', ERRORTEXT.ERRORHEADER.EDITFORMERROR.ERROREND.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDEDPRODUCTNOTFOUNDERROR', ERRORTEXT.ERRORHEADER.PRODUCTNOTFOUNDERROR.ERROREND.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDUSERNOTFOUNDERROR', ERRORTEXT.ERRORHEADER.USERNOTFOUNDERROR.ERROREND.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDEDDBQUERYERROR', ERRORTEXT.ERRORHEADER.DBQUERYERROR.ERROREND.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDEDDBCONNECTIONERROR', ERRORTEXT.ERRORHEADER.DBCONNECTIONERROR.ERROREND.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDEMPTYFIELDSERROR', ERRORTEXT.ERRORHEADER.EMPTYFIELDSERROR.ERROREND.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDEMPTYUSERNAME', ERRORTEXT.EMPTYUSERNAME.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDEMPTYPASSWORD', ERRORTEXT.EMPTYPASSWORD.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDEMPTYNAME', ERRORTEXT.EMPTYNAME.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDEMPTYSURNAME', ERRORTEXT.EMPTYSURNAME.CLOSETEXT);
    \define(__NAMESPACE__.'\FORMEDONLYVISITORSERROR', FLEXDIV.ERRORTEXT.ONLYVISITORSERROR.CLOSETEXT.CLOSEDIV);
    \define(__NAMESPACE__.'\FORMEDONLYLOGGEDERROR', FLEXDIV.ERRORTEXT.ONLYLOGGEDERROR.CLOSETEXT.CLOSEDIV);
    \define(__NAMESPACE__.'\FORMEDONLYAUTHORIZEDERROR', FLEXDIV.ERRORTEXT.ONLYAUTHORIZEDERROR.CLOSETEXT.CLOSEDIV);

    /** destroySession: Function that destroys a session securely.
     * @author gerardgg
     * @version 1.0
     * @date 08/10/2020 
     */
    function destroySession() {
        if(\session_id()) {
            unset($_SESSION);
            \session_destroy();
        }
    }
}

// EOF