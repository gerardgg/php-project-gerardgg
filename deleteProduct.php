<?php

\session_start();

// Verify permissions
require_once 'Views/assets/permissions/verifyStaff.php';
// Provides access to namespaces to work properly
require_once 'Logic/editData.php';
// Needed namespaces for required functions or constants
use GoldHotel\Edit\Delete\Products as DeleteProducts;

DeleteProducts\masterDeleteProduct();