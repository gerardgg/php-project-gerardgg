<?php

// Verify permissions
require_once 'Views/assets/permissions/verifyAdmin.php';

// Provides access to namespaces to work properly
require_once 'Logic/editData.php';
// Needed namespaces for required functions or constants
use GoldHotel\Edit\Modificator\Users as UserModificator;

UserModificator\masterSaveChanges();