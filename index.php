<?php // Provides access to view ?>
<?php require_once 'Views/header.php'; ?>
<?php // Displays permission errors ?>
<?php require_once 'Views/assets/errors/permError.php' ?>
    <h1>Gold Hotel 3*</h1>
    <img src="./Images/hotel1" id="hotelImage" alt="hotel image">
    <div class="container">
    <h2>Presentation</h2>
    <p class="justifiedText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a justo sapien. Curabitur turpis diam, convallis at malesuada et, hendrerit quis orci. Proin euismod sollicitudin nibh, ut cursus lacus feugiat non. Ut facilisis, orci non faucibus consectetur, odio nibh posuere lorem, eu accumsan turpis eros ac nisi. Cras egestas ligula purus, non gravida neque suscipit a. Donec ornare, magna at dictum iaculis, massa nunc accumsan nunc, ut ornare justo turpis vitae augue. Phasellus vehicula accumsan viverra.</p>
    <h2>Welcome</h2>
    <p class="justifiedText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a justo sapien. Curabitur turpis diam, convallis at malesuada et, hendrerit quis orci. Proin euismod sollicitudin nibh, ut cursus lacus feugiat non. Ut facilisis, orci non faucibus consectetur, odio nibh posuere lorem, eu accumsan turpis eros ac nisi. Cras egestas ligula purus, non gravida neque suscipit a. Donec ornare, magna at dictum iaculis, massa nunc accumsan nunc, ut ornare justo turpis vitae augue. Phasellus vehicula accumsan viverra.</p>
    </div>
<?php // Provides access to view ?>
<?php require_once 'Views/footer.php'; ?>