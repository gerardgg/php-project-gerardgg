<?php // Provides access to namespaces to work properly ?>
<?php require_once 'Logic/editData.php'; ?>
<?php // Needed namespaces for required functions or constants ?>
<?php use GoldHotel\Edit\Add\Products as AddProducts; ?>
<?php // Provides access to view ?>
<?php require_once 'Views/header.php'; ?>
<?php // Verify permissions ?>
<?php require_once 'Views/assets/permissions/verifyStaff.php' ?>
<h1>Add menu</h1>
<form action="addProduct.php" method="POST">
    <fieldset>
    <div>
        <label for="name">Name:</label>
        <input type="text" name="productName" id="name">
    </div>
    <div>
        <label for="category">Category:</label>
        <select name="productCategories" id="category">
            <?= AddProducts\outputCategories(); ?>
        </select>
    </div>
    <div>
        <label for="price">Price:</label>
        <input type="number" name="productPrice" id="price">
    </div>
    </fieldset>
    <input class="goldButton" type="reset" value="Reset">
    <button class="goldButton" type="submit" name="confirmEdit" value="addProduct">Submit</button>
</form>
<?php // Provides access to view ?>
<?php require_once 'Views/footer.php'; ?>