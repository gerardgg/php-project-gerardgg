<?php // Provides access to namespaces to work properly ?>
<?php require_once 'Logic/products.php'; ?>
<?php // Needed namespaces for required functions or constants ?>
<?php use GoldHotel\Products\Menu as Menu; ?>
<?php // Provides access to view ?>
<?php require_once 'Views/header.php'; ?>
<?php // Verify permissions ?>
<?php require_once 'Views/assets/permissions/verifyLogged.php' ?>
<h1>Menu</h1>
<?php Menu\masterPrintMenu();?>
<?php // Provides access to view ?>
<?php require_once 'Views/footer.php'; ?>