<?php // Provides access to namespaces to work properly ?>
<?php require_once 'Logic/editData.php' ?>
<?php // Needed namespaces for required functions or constants ?>
<?php use GoldHotel\Edit\Generics as EditGenerics ?>
<?php use GoldHotel\Edit\Views\Products as EditViewProducts ?>
<?php // Provides access to view ?>
<?php require_once 'Views/header.php'; ?>
<?php // Verify permissions ?>
<?php require_once 'Views/assets/permissions/verifyStaff.php' ?>
<h1>Edit menu</h1>
<?php $outputableData = EditViewProducts\loadTargeEditProductData(); ?>
<?php echo $outputableData; ?>
<?php // Provides access to view ?>
<?php require_once 'Views/footer.php'; ?>