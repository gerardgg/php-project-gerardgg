<?php // Provides access to namespaces to work properly ?>
<?php require_once 'Logic/products.php'; ?>
<?php // Needed namespaces for required functions or constants ?>
<?php use GoldHotel\Products\DayMenu as DayMenu; ?>
<?php // Provides access to view ?>
<?php require_once 'Views/header.php'; ?>
<h1>Day Menu</h1>
<?php DayMenu\masterPrintDayMenu(); ?>
<?php // Provides access to view ?>
<?php require_once 'Views/footer.php'; ?>