<?php 

    \session_start(); 

    // Provides access to namespaces to work properly
    require 'Logic/users.php';
    // Needed namespaces for required functions
    use GoldHotel\Users\LogOut as LogOut;
    // Verify permissions 
    require_once 'Views/assets/permissions/verifyLogged.php';
    // Provides access to namespaces to work properly

    $resultLogOut = LogOut\logOut();

    \session_start();

    $_SESSION['logOut'] = $resultLogOut;

    // Redirection to home
    \header('Location: index.php');
    exit();

// EOF
