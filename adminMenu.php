<?php // Provides access to namespaces to work properly ?>
<?php require_once 'Logic/administration.php' ?>
<?php use GoldHotel\Administration\Menu as admMenu ?>
<?php // Provides access to view ?>
<?php require_once 'Views/header.php'; ?>
<?php // Verify permissions ?>
<?php require_once 'Views/assets/permissions/verifyAdmin.php' ?>
<h1>Menu administration</h1>
<a href="addMenu.php"><button class="goldButton blockandcenter">Add product</button></a>
<?php if(isset($_SESSION['editSuccessful'])): ?>
    <?= $_SESSION['editSuccessful'] ?>
    <?php unset($_SESSION['editSuccessful']);?>
<?php else: ?>
    <?php if(isset($_SESSION['editError'])): ?>
        <?= $_SESSION['editError'] ?>
        <?php unset($_SESSION['editError']); ?>
    <?php endif; ?>
    <?php if(isset($_SESSION['editErrorProductNotFound'])): ?>
        <?= $_SESSION['editErrorProductNotFound'] ?>
        <?php unset($_SESSION['editErrorProductNotFound']); ?>
    <?php endif; ?>
<?php endif; ?>
<?php // Provides access to view errors ?>
<?php require_once 'Views/assets/errors/dbErrors.php'; ?>
<?php $menu = admMenu\masterEditableMenu(); echo $menu; ?>
<?php // Provides access to view ?>
<?php require_once 'Views/footer.php'; var_dump($_SESSION); ?>