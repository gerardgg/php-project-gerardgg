<?php // Provides access to view ?>
<?php // Provides access to view ?>
<?php require_once 'Views/header.php'; ?>
<?php // Needed namespaces for required functions or constants ?>
<?php use GoldHotel\Generics as GlobalGenerics; ?>
<?php // Provides access to namespaces to work properly ?>
<?php require_once 'Logic/users.php' ?>
<?php // Needed namespaces for required functions or constants ?>
<?php use GoldHotel\Users\LogIn as LogIn ?>
    <div class="loginForm">
        <h2>Login</h2>
        <form method="POST" action="<?= \htmlentities($_SERVER['PHP_SELF']); ?>">
            <?php if(isset($_SESSION['userInfo']['username'])): ?>
                <?php unset($_SESSION['formResults']); ?>
                <?php \header('Location: index.php'); ?>
            <?php else: ?>
            <?php require_once 'Views/assets/login/verifyConnectionErrors.php'; ?>
            <?php if(isset($_SESSION['invalidCredentials'])): ?>
                <?= GlobalGenerics\ERRORTEXT.$_SESSION['invalidCredentials'].GlobalGenerics\CLOSETEXT.
                GlobalGenerics\ERRORTEXT.'Introduced user: '.$_SESSION['formResults']['username'].GlobalGenerics\CLOSETEXT.
                GlobalGenerics\ERRORTEXT.'Introduced password: '.$_SESSION['formResults']['password'].GlobalGenerics\CLOSETEXT ?>
                <?php unset($_SESSION['invalidCredentials'])?>
            <?php endif; ?>
            <fieldset>
                <legend>Introduce your data</legend>
                <?php $emptyFieldsFlag = isset($_SESSION['formResults']) ? true : false; ?>
                <?php if($emptyFieldsFlag && $_SESSION['formResults']['username'] == null): echo GlobalGenerics\ERRORTEXT.'Username is required'.GlobalGenerics\CLOSETEXT;  endif; ?>
                <input type="text" name="username" id="username" placeholder="Username">
                <?php if($emptyFieldsFlag && $_SESSION['formResults']['password'] == null): echo GlobalGenerics\ERRORTEXT.'Password is required'.GlobalGenerics\CLOSETEXT;  endif; ?>
                <input type="password" name="password" id="password" placeholder="Password">
                <div class="loginButtons">
                    <a href="register.php" class="goldButton removeDecoration">Create account</a>
                    <input type="submit" name="submitButton" value="Submit" class="goldButton">
                </div>
            </fieldset>
        </form>
        <?php if(isset($_SESSION['formResults'])): ?>
            <?php unset($_SESSION['formResults']); ?>
        <?php endif; ?>
        <?php if(isset($_POST['submitButton'])): LogIn\logInMaster(); \header("Refresh:0"); ?>
        <?php endif; ?>
    </div>
        <?php endif;?>
<?php // Provides access to view ?>
<?php require_once 'Views/footer.php'?>